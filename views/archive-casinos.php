<?php if ( have_posts() ) : ?>

	<?php echo $map; ?>

	<hr>

	<div class="entry-content">
		<table>
			<thead>
				<tr>
					<th>Casino</th>
					<th>Address</th>
					<th>Destination</th>
				</tr>
			</thead>
			<tbody>

				<?php	while ( have_posts() ) : the_post(); ?>

					<?php $casinos->query_id( get_the_ID() ); ?>

					<tr>
						<td>
							<strong>
								<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
							</strong>
							<?php edit_post_link( __( '<br>Edit', 'twentytwelve' ), '<span class="edit-link">', '</span>' ); ?>
						</td>
						<td><?php echo types_render_field( 'address', array('raw'=>true) ); ?></td>
						<td>
							<?php echo $casinos->parent_link( get_the_ID() ); ?>
						</td>
					</tr>

				<?php endwhile; ?>

			</tbody>
		</table>

		<nav id="nav-below" class="navigation" role="navigation">
			<div class="nav-previous alignleft"><?php echo previous_posts_link( __( '<span class="meta-nav">&larr;</span> Previous Page', 'twentytwelve' ) ); ?></div>
			<div class="nav-next alignright"><?php echo next_posts_link( __( 'Next Page <span class="meta-nav">&rarr;</span>', 'twentytwelve' ) ); ?></div>
		</nav>


	</div>

	<?php $casinos->add_markers(); ?>

<?php else : ?>
	<?php // get_template_part( 'content', 'none' ); ?>
<?php endif; ?>


<?php
// wp_enqueue_script( 'gmaps-api-js', 'http://maps.google.com/maps/api/js?sensor=true', array( 'jquery' ) );
// wp_enqueue_script( 'gmaps-js', get_stylesheet_directory_uri() . '/js/gmaps.js', array( 'jquery' ) );
?>