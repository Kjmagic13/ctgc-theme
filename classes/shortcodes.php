<?php

/**
* Shortcodes
*/
class Shortcodes extends Theme
{
	
	public function __construct()
	{
		foreach ( get_class_methods(__CLASS__) as $key => $method ) {
			if ( $method == '__construct' ) { continue; }
			add_shortcode( $method, array($this, $method) );
		}

	}

	//
	function custom_events_list() {

		global $post;

		$out = "
		<figure>
			<a href=\"%event_url%\">%event_thumbnail{large}%</a>
		</figure>
		<figcaption>
			<header>
				<h3><a href=\"%event_url%\">%event_title%</a></h3>
				<small>
					%start{M jS Y}% - %end{M jS Y}% |
					<a href=\"%event_venue_url%\">%event_venue%</a>
				</small>
			</header>
			%event_excerpt%

			<section class=\"event-learnmore\">
				<a href=\"%event_url%\">
					Learn More
				</a>
			</section>
		</figcaption>
		";

		return do_shortcode('[eo_events showpastevents=false]'.$out.'[/eo_events]');

	}

	// [destination_list type="type"]
	function destination_list($atts) {

		extract( shortcode_atts( array('type' => null), $atts ) );

		$args = array(
			'post_type' => 'destinations',
			'dest-types' => $type,
			'orderby' => 'title',
			'order' => 'ASC',
			'posts_per_page' => -1,

			);
		$query = new WP_Query( $args );

		$dests = array();

		if ( $query->have_posts() ) { while ( $query->have_posts() ) { $query->the_post();
			$title = get_the_title();
			$link = get_permalink();
			$string = "<a href='$link'>$title</a>";
			$dests[] = $string;
		}}

		return $out = implode(" | ", $dests);

	}

	// [flight_selects]
	function flight_selects() {

		$flights = new Flights();

		$out = '
		<div id="flight-widget">'
			.$flights->select_dates('Departure Date').'<br>'
			.$flights->select_city('Destination', 'arriving-cities').'<br>'
			.$flights->select_city('Departing City', 'departing-cities').'
		</div>
		';

		return $out;

	}

}