<?php

/**
* Flights
*/
class Flights extends Theme
{
	
	function __construct()
	{
		//parent::__construct();

		add_filter( 'wp_insert_post_data', array($this, 'prevent_future_type') );
		remove_action( 'future_post', '_future_post_hook' );
		// add_action( 'generate_rewrite_rules', array($this, 'rewrite_rules') );
		add_action( 'pre_get_posts', array($this, 'alter_flights_query') );
	}

	function alter_flights_query($query)
	{
		if ($query->query['post_type'] == "flights" || array_key_exists('arriving-cities', $query->query) || array_key_exists('departing-cities', $query->query) ) {
			if ( !$query->is_main_query() ) { return; }
			$query->set('orderby', 'date');
			$query->set('order', 'ASC');
			$query->set('posts_per_page', 25);
			$query->set('date_query', array(
				array(
					'after'    => array(
						'year'  => date(Y),
						'month' => date(m),
						'day'   => date(d),
						),
					'inclusive' => false,
					),
				)
			);
		}

	}

	// Custom post type specific rewrite rules
	public function rewrite_rules($wp_rewrite)
	{
		$rules = $this->generate_date_archives('flights', $wp_rewrite);
		$wp_rewrite->rules = $rules + $wp_rewrite->rules;
		return $wp_rewrite;
	}

	// Disable Scheduling of Flights
	public function prevent_future_type( $post_data )
	{
		if ( $post_data['post_status'] == 'future' && $post_data['post_type'] == 'flights' ){
			$post_data['post_status'] = 'publish';
		}
		return $post_data;
	}

	// select flights by date
	public function select_dates($def='Select Date')
	{
		$args = array(
			'post_type' => 'flights',
			'order' => 'ASC',
			'orderby' => 'date',
			'posts_per_page' => -1
			);
		$query = new WP_Query( $args );

		$out = "<select name='flight-date-dropdown' onchange='document.location.href=this.options[this.selectedIndex].value;'>";
		$out .= "<option value=''>$def</option>";
		$dates = array();
		if ( $query->have_posts() ) { while ( $query->have_posts() ) { $query->the_post();
			if ( in_array(get_the_date(), $dates) ) 		{ continue; }
			if ( time() > strtotime(get_the_date()) ) 	{ continue; }
			// $link = home_url() . '/flights/' . get_the_time('Y') . '/' . get_the_time('m') . '/' . get_the_time('d');
			$link = home_url() . '/' . get_the_time('Y') . '/' . get_the_time('m') . '/' . get_the_time('d') . '/?post_type=flights';
			$out .= "<option value='$link'>" . get_the_date() . "</option>";
			$dates[] = get_the_date();
		}}
		$out .= "</select>";

		return $out;
	}

	// select flights by city
	public function select_city($def='Select City', $method='arriving-cities')
	{
		// include only terms containing future flights
		$include_terms[] = true;
		global $wpdb;
		$time = date("Y-m-d", time());
		$sql = "SELECT t.term_id
		FROM $wpdb->terms t
		INNER JOIN $wpdb->term_taxonomy y ON t.term_id = y.term_id
		INNER JOIN $wpdb->term_relationships r ON y.term_taxonomy_id = r.term_taxonomy_id
		INNER JOIN $wpdb->posts p ON r.object_id = p.ID
		WHERE taxonomy = '$method' AND DATE(post_date) > '$time'
		";
		$results = $wpdb->get_results($sql);
		foreach ($results as $key => $obj) {
			$include_terms[] = $obj->term_id;
		}

		// get terms
		$taxonomies = array( $method );
		$args = array(
			'orderby'       => 'name', 
			'order'         => 'ASC',
			'hide_empty'    => true, 
			'exclude'       => array(), 
			'exclude_tree'  => array(), 
			'include'       => $include_terms,
			'number'        => '', 
			'fields'        => 'all', 
			'slug'          => '', 
			'parent'        => '',
			'hierarchical'  => true, 
			'child_of'      => 0, 
			'get'           => '', 
			'name__like'    => '',
			'pad_counts'    => false, 
			'offset'        => '', 
			'search'        => '', 
			'cache_domain'  => 'core'
			);
		$cities = get_terms( $taxonomies, $args );

		$out = "<select name='flight-$method-dropdown' onchange='document.location.href=this.options[this.selectedIndex].value;'>";
		$out .= "<option value=''>$def</option>";
		foreach ( $cities as $city ) {
			$link = get_term_link($city);
			$out .= "<option value='$link'>$city->name</option>";
		}
		$out .= "</select>";

		return $out;
	}

}
