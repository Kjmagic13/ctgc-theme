<?php

/**
* Sidebars
*/
class Sidebars extends Theme
{

	public function __construct()
	{
		// parent::__construct();

		foreach ($this->register_sidebars() as $key => $sibebar) {
			register_sidebar($sibebar);
		}
	}

	public function register_sidebars()
	{
		$sidebars[] = array(
			'name' => __( 'Third Front Page Widget Area', 'twentytwelve' ),
			'id' => 'sidebar-4',
			'description' => __( 'Appears when using the optional Front Page template with a page set as Static Front Page', 'twentytwelve' ),
			'class'				=> 'sidebar-class',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget' => '</aside>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>', );
		
		return $sidebars;
	}

}

$sidebars = new Sidebars();
