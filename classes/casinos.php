<?php

/**
* Casinos
*/
class Casinos extends Theme
{

	var $queried_ids = array();
	
	function __construct( $query_args=array() )
	{
		add_action( 'pre_get_posts', array($this, 'pre_get_posts') );
	}

	function the_loop( $query_args=array() )
	{
		if ( empty($query_args) ) { $query_args = $this->default_query_args(); }

		$variables = array(
			'casinos' => new Casinos,
			'wp_query' => query_posts( $query_args ),
			'map' => $this->map(),
			);

		parent::render_view('archive-casinos', $variables, true);

		wp_reset_query();
	}

	function default_query_args()
	{
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

		$query_args = array(
			'post_type' => 'casinos',
			'posts_per_page' => get_option('posts_per_page'),
			'paged' => $paged,
			// 'orderby' => 'meta_value',
			// 'meta_key' => '_wpcf_belongs_destinations_id',
			// 'order' => 'ASC',
			);

		return $query_args;
	}

	function query_id($id)
	{
		array_push($this->queried_ids, $id);
	}

	function map()
	{
		$out = "
		<div id=\"casino_geo_map\" style=\"width: 100%; height: 325px;\"></div>
		<script type=\"text/javascript\">
			var map = new GMaps({ div: '#casino_geo_map', lat: 0, lng: 0 });
		</script>
		";

		return $out;
	}

	function parent_link($id)
	{
		$parent_id = get_post_meta( $id, '_wpcf_belongs_destinations_id', true );
		$link = get_permalink($parent_id);
		$title = get_the_title($parent_id);

		$out = "<a href=\"$link\">$title</a>";
		return $out;
	}

	function add_markers()
	{
		$out = "<script type=\"text/javascript\">";

		foreach ($this->queried_ids as $key => $id) {
			// $address = types_render_field( 'address', array('raw'=>true) );
			$address = get_post_meta( $id, 'wpcf-address', true );
			$link = get_permalink($id);
			$title = get_the_title($id);

			$out .= "
			GMaps.geocode({
				address: \"$address\",
				callback: function(results, status) {
					if (status == 'OK') {
						var latlng = results[0].geometry.location;
						map.addMarker({
							lat: latlng.lat(),
							lng: latlng.lng(),
							infoWindow: {
								content: \"<p><a href='$link'>$title</a></p>\"
							},
						});
						map.fitZoom();
					}
				}
			});
			";
		}

		$out .= "</script>";

		echo $out;
	}

	function pre_get_posts($query)
	{
		global $post;

		if ( $query->query['post_type'] != 'casinos' || $post->post_type == 'destinations' ) {
			remove_filter( 'posts_join', array($this, 'join_destination_meta') );
			remove_filter( 'posts_orderby', array($this, 'order_by_destination') );
		} else {
			add_filter( 'posts_join', array($this, 'join_destination_meta') );
			add_filter( 'posts_orderby', array($this, 'order_by_destination') );
		}
	}

	function join_destination_meta($join)
	{
		global $wpdb;
		$join = "
		INNER JOIN $wpdb->postmeta m ON ($wpdb->posts.ID = m.post_id AND m.meta_key = '_wpcf_belongs_destinations_id')
		INNER JOIN $wpdb->posts d ON m.meta_value = d.ID
		";
		return $join;
	}

	function order_by_destination($orderby_statement)
	{
		global $wpdb, $wp_query;
		$orderby_statement = "d.post_title ASC, $wpdb->posts.post_title ASC";
		return $orderby_statement;
	}

}