<?php

class ThemeCustomizer extends Theme
{
	
	function __construct()
	{
		add_action( 'customize_register', array($this, 'themename_customize_register') );
		add_action( 'wp_head', array($this, 'tcx_customizer_css') );
	}


	function themename_customize_register($wp_customize){
		// Main Background Color
		$wp_customize->add_setting(
			'tcx_main_bg_color',
			array(
				'default'      => '#251f16',
				'transport'    => 'refresh',
				'capability'   => 'edit_theme_options',
				)
			);
		$wp_customize->add_control( 
			new WP_Customize_Color_Control( 
				$wp_customize, 
				'main_bg_color',
				array(
					'label'      => 'Main BG Color',
					'section'    => 'colors',
					'settings'   => 'tcx_main_bg_color',
					) ) 
			);

		// Main Text Color
		$wp_customize->add_setting(
			'tcx_main_text_color',
			array(
				'default'      => '#9b835c',
				'transport'    => 'refresh',
				'capability'   => 'edit_theme_options',
				)
			);
		$wp_customize->add_control( 
			new WP_Customize_Color_Control( 
				$wp_customize, 
				'main_text_color',
				array(
					'label'      => 'Main Text Color',
					'section'    => 'colors',
					'settings'   => 'tcx_main_text_color',
					) ) 
			);

		// Widget Headers & Breadcrumbs Color
		$wp_customize->add_setting(
			'tcx_widget_header_color',
			array(
				'default'      => '#3a362e',
				'transport'    => 'refresh',
				'capability'   => 'edit_theme_options',
				)
			);
		$wp_customize->add_control( 
			new WP_Customize_Color_Control( 
				$wp_customize, 
				'widget_header_color',
				array(
					'label'      => 'Widget Headers Color',
					'section'    => 'colors',
					'settings'   => 'tcx_widget_header_color',
					) ) 
			);
	}


	function tcx_customizer_css() {
		?>
		<style type="text/css">

			.site {
				background-color: <?php echo get_theme_mod( 'tcx_main_bg_color' ); ?>;
				color: <?php echo get_theme_mod( 'tcx_main_text_color' ); ?>;
			}
			#secondary aside h3, #breadcrumbs {
				background-color: <?php echo get_theme_mod( 'tcx_widget_header_color' ); ?>;
			}

		</style>
		<?php
	}

}
