<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

global $theme;
?>
</div><!-- #main .wrapper -->
</div><!-- #page -->

<nav id="bottom-menu" class="desktop">
	<?php wp_nav_menu( array( 'theme_location' => 'casinos-menu', 'menu_class' => 'nav-menu' ) ); ?>
</nav>

<footer id="footer">
	<div id="footer-wrap">

		<?php echo $theme->footer_casino_logos(); ?>

		<section id="copright">
			COPYRIGHT &copy; 2004-<?php echo date("Y"); ?> 
			<a href="#">CASINOS THE GRAND COLLECTION LLC</a> |
			Toll Free: <a href="tel:8668182646">866.818.COIN</a> (2646) |
			<a href="mailto:info@casinostgc.com">info@casinostgc.com</a> |
			<a href="http://www.richiebryan.com" target="_blank">Webmaster</a>
			<a class="facebook-icon" href="http://www.facebook.com/pages/CASINOS-The-Grand-Collection/124872280941935" target="_new">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/facebook_icon.png" border="0" style="vertical-align: middle; margin-left:10px;">
			</a>
		</section>

	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>