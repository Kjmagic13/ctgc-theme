<?php get_header(); ?>

<section id="primary" class="site-content">
	<div id="content" role="main">

		<?php if ( have_posts() ) : ?>

			<div class="entry-content">

				<table>
					<thead>
						<tr>
							<th>Flight</th>
							<th>Arrival</th>
							<th>Departure</th>
						</tr>
					</thead>
					<tbody>

						<?php while ( have_posts() ) : the_post(); ?>
							<tr>
								<td><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></td>
								<td><?php echo get_the_date(); ?></td>
								<td><?php echo do_shortcode('[types field="return-date" style="text" format="F j, Y"][/types]'); ?></td>
							</tr>
						<?php endwhile; ?>

					</tbody>
				</table>

			</div>

			<nav id="nav-below" class="navigation" role="navigation">
				<div class="nav-previous alignleft"><?php previous_posts_link( __( '<span class="meta-nav">&larr;</span> Previous Page', 'twentytwelve' ) ); ?></div>
				<div class="nav-next alignright"><?php next_posts_link( __( 'Next Page <span class="meta-nav">&rarr;</span>', 'twentytwelve' ) ); ?></div>
			</nav>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; wp_reset_postdata(); ?>

	</div><!-- #content -->
</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>