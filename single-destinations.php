<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

wp_enqueue_script( 'gmaps-api-js', 'http://maps.google.com/maps/api/js?sensor=true', array( 'jquery' ) );
wp_enqueue_script( 'gmaps-js', get_stylesheet_directory_uri() . '/js/gmaps.js', array( 'jquery' ) );

get_header(); ?>

<div id="primary" class="site-content">
	<div id="content" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<header class="entry-header">
				<h1 class="entry-title"><?php the_title(); ?></h1>
			</header><!-- .entry-header -->

			<article>
				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->
			</article>

			<?php
			$args = array(
				'post_type' => 'casinos',
				'posts_per_page' => -1,
				'orderby' => 'name',
				'order' => 'ASC',
				'meta_query' => array(
					array(
						'key' => '_wpcf_belongs_destinations_id',
						'value' => get_the_ID()
						)
					)
				);
			$casinos = new Casinos(); $casinos->the_loop($args);
			?>

			<?php // get_template_part( 'content', get_post_format() ); ?>

			<?php comments_template( '', true ); ?>

		<?php endwhile; // end of the loop. ?>

	</div><!-- #content -->
</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>