<?php

class Theme
{

	var $options;
	var $shortcodes;
	var $casinos;

	public function __construct()
	{
		// Require Classes
		$inc_func_path = get_stylesheet_directory().'/classes';
		foreach( glob( "$inc_func_path/*.php" ) as $filename ){
			require_once($filename);
		}

		// set theme variables
		// $this->options = get_option( 'theme_options' );
		$this->shortcodes = new Shortcodes;
		// $this->casinos = new Casinos;
		$this->theme_cust = new ThemeCustomizer;
		$this->flights = new Flights;

		// Enqueue Theme Styles & Scripts
		add_action( 'wp_enqueue_scripts', array( $this, 'theme_enqueues' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueues' ) );
		add_filter( 'mce_css', array( $this, 'tinymce_css' ) );

		// Enable Shortcodes in Widgets
		add_filter( 'widget_text', 'do_shortcode' );

		// Add styles/classes to the "Styles" drop-down  
		add_filter( 'tiny_mce_before_init', array( $this, 'custom_mce_style_formats' ) );
		add_filter( 'mce_buttons_2', array( $this, 'styleselect_tinymce_buttons' ) );

		// CSV Helper
		// add_action( 'admin_enqueue_scripts', array($this, 'csv_helper') );
	}

	// Enqueue Theme Styles & Scripts
	public function theme_enqueues()
	{
		//scripts
		wp_enqueue_script( 'gmaps-api-js', 'http://maps.google.com/maps/api/js?sensor=true', array( 'jquery' ) );
		wp_enqueue_script( 'gmaps-js', get_stylesheet_directory_uri() . '/js/gmaps.js', array( 'jquery' ) );
		wp_enqueue_script( 'theme-functions-js', get_stylesheet_directory_uri() . '/js/functions.js', array( 'jquery' ) );
		//styles
	}

	// Enqueue Theme Styles & Scripts
	public function admin_enqueues()
	{
		wp_enqueue_style( 'custom_wp_admin_css', get_stylesheet_directory_uri() . '/css/admin-style.css', false );
	}

	function render_view($file, $variables = array(), $echo=true)
	{
		extract($variables);
		ob_start();
		include get_stylesheet_directory() . '/views/' . $file . '.php';
		$renderedView = ob_get_clean();

		if ($echo) { echo $renderedView; } else { return $renderedView; }
	}

	// CSV Helper
	public function csv_helper($hook)
	{
		if( 'toplevel_page_wp-ultimate-csv-importer/index' != $hook ) { return; }
		if( $_GET['__module'] == 'custompost' && $_GET['step'] == 'mapping_settings' ) {
			wp_enqueue_script( 'csv-helper', get_stylesheet_directory_uri() . '/js/csv-helper.js', array( 'jquery' ) );
		}
	}

	

	public function custom_mce_style_formats($init)
	{  
		$style_formats = array(  
			// array(  
			// 	'title' => 'Lead',
			// 	'selector' => 'p',
			// 	'classes' => 'lead'
			// 	),
			array(  
				'title' => 'Responsive Video',
				'block' => 'div',
				'classes' => 'responsive-video'
				),
			);  

		$init['style_formats'] = json_encode( $style_formats );

		return $init;  

	}

	function styleselect_tinymce_buttons($buttons)
	{
		array_unshift($buttons, 'styleselect');
		return $buttons;
	}

	// Custom MCE Styles
	function tinymce_css($wp)
	{
		// $wp .= ',' . get_bloginfo('stylesheet_url');
		$wp .= ',' . get_bloginfo('stylesheet_directory') . '/css/tinymce.css';
		return $wp;
	}

	// footer casino logos
	public function footer_casino_logos()
	{ // todo
		$path = '/images/casino-logos/';
		$dir = opendir( get_stylesheet_directory() . $path );
		while (($filename = readdir($dir)) !== false){
			if($filename != "." && $filename != ".." && $filename != ".DS_Store" && $filename != "thumbs"){
				$filenames[] = $filename;
			}
		}
		sort($filenames);

		$out = '<section id="footer-logos">';
		foreach ($filenames as $file) {
			$out .= '<img src="' . get_stylesheet_directory_uri() . $path . $file . '">';
		}
		$out .= '</section>';

		return $out;
	}

	// Generate date archive rewrite rules for a given custom post type
	// may need to reactivate permalinks
	public function generate_date_archives($cpt, $wp_rewrite)
	{
		$rules = array();

		$post_type = get_post_type_object($cpt);
		$slug_archive = $post_type->has_archive;
		if ($slug_archive === false) return $rules;
		if ($slug_archive === true) {
			$slug_archive = $post_type->name;
		}

		$dates = array(
			array(
				'rule' => "([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})",
				'vars' => array('year', 'monthnum', 'day')),
			array(
				'rule' => "([0-9]{4})/([0-9]{1,2})",
				'vars' => array('year', 'monthnum')),
			array(
				'rule' => "([0-9]{4})",
				'vars' => array('year'))
			);

		foreach ($dates as $data) {
			$query = 'index.php?post_type='.$cpt;
			$rule = $slug_archive.'/'.$data['rule'];

			$i = 1;
			foreach ($data['vars'] as $var) {
				$query.= '&'.$var.'='.$wp_rewrite->preg_index($i);
				$i++;
			}

			$rules[$rule."/?$"] = $query;
			$rules[$rule."/feed/(feed|rdf|rss|rss2|atom)/?$"] = $query."&feed=".$wp_rewrite->preg_index($i);
			$rules[$rule."/(feed|rdf|rss|rss2|atom)/?$"] = $query."&feed=".$wp_rewrite->preg_index($i);
			$rules[$rule."/page/([0-9]{1,})/?$"] = $query."&paged=".$wp_rewrite->preg_index($i);
		}
		return $rules;
	}

}

$theme = new Theme();
