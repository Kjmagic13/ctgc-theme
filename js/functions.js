function geocode_add_marker(map, address, icon)
{
	icon = icon || "http://maps.google.com/mapfiles/ms/icons/green-dot.png";
	// "http://maps.google.com/mapfiles/ms/icons/blue-pushpin.png"
	// "http://maps.google.com/mapfiles/ms/icons/red-pushpin.png"

	GMaps.geocode({
		address: address,
		callback: function(results, status) {
			if (status == 'OK') {
				var latlng = results[0].geometry.location;
				map.addMarker({
					lat: latlng.lat(),
					lng: latlng.lng(),
					icon: { url: icon }
				});
				map.fitZoom();
				var arr = [parseFloat(latlng.lat()), parseFloat(latlng.lng())];
				jQuery(document).ready(function($){
					var latlngs = $(map.el).data('latlng');
					latlngs.push(arr);
					$(map.el).data('latlng', latlngs);
				});
			}
		}
	});
}