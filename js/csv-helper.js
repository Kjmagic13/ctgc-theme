jQuery(document).ready(function($){

	if (confirm('Would you like to use presets defined by the webmaster to import Flights?')) {

		$('#custompostlist option[id=flights]').attr('selected', 'selected');
		$('#importallwithps option[value=1]').attr('selected', 'selected');

		$('#mapping0 option[value=arriving-cities]').attr('selected', 'selected');
		$('#mapping2 option[value=post_date]').attr('selected', 'selected');

		$('#mapping4 option[value=add_custom4]').attr('selected', 'selected').trigger('change');
		$('#textbox4').val('wpcf-return-date');

		$('#mapping6 option[value=add_custom6]').attr('selected', 'selected').trigger('change');
		$('#textbox6').val('wpcf-departing-airport');

		$('#mapping7 option[value=departing-cities]').attr('selected', 'selected');

		// $('#goto_importer_setting').trigger('click');
		// alert('Thank you. Click the light blue "Next" button to advance to the next screen.');

	} else {
		return false;
	};
	

});